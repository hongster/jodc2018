<?php

namespace App\Event;

use App\Event\Calling;
use Illuminate\Support\ServiceProvider;

class CallingServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('calling', function ($app) {
            return new Calling();
        });
    }

}