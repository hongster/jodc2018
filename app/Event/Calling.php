<?php

namespace App\Event;

use Cache;

class Calling {

    /** Cache key.  */
    const KEY_CALLING = "event.calling";

    /**
     * List current calling events.
     * @return array Event IDs.
     */
    public function events()
    {
        return Cache::get(self::KEY_CALLING, []);
    }

    /**
     * Add event to calling list.
     * @param int $eventId
     */
    public function call($eventId)
    {
        $list = $this->events();
        if (!in_array($eventId, $list))
        {
            $list[] = $eventId;
            Cache::forever(self::KEY_CALLING, $list);
        }

        return $list;
    }

    /**
     * Remove event from calling list.
     * @param int $eventId
     */
    public function uncall($eventId)
    {
        $list = $this->events();
        $key = array_search($eventId, $list);

        if ($key !== false)
        {
            unset($list[$key]);
            Cache::forever(self::KEY_CALLING, $list);
        }

        return $list;
    }

    /**
     * Clear all events from calling list.
     */
    public function clear()
    {
        Cache::forever(self::KEY_CALLING, []);
    }

    /**
     * Check if an event is in calling list.
     * @return bool
     */
    public function isCallingEvent($eventId)
    {
        $list = $this->events();
        $key = array_search($eventId, $list);

        return $key !== false;   
    }

}