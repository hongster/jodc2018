<?php 

namespace App\Event;

use Cache;

/**
 * At any one time, only 1 event is undergoing prize presentation.
 */
class Presentation {

    /** Cache key */
    const KEY_PRESENTATION = "event.presentation";

    /**
     * Return current presenting event.
     * @return int $eventId
     */
    public static function event()
    {
        return Cache::get(self::KEY_PRESENTATION);
    }

    /**
     * Check if prize presentation is ongoing.
     * @param int $eventId
     * @return bool
     */
    public static function isPresenting($eventId)
    {
        return Cache::get(self::KEY_PRESENTATION) == $eventId;
    }

    /**
     * Mark this event under prize presentation.
     * @param int $eventId
     */
    public static function presenting($eventId)
    {
        Cache::forever(self::KEY_PRESENTATION, $eventId);
    }

    /**
     * No prize presentation at this moment.
     */
    public static function clear()
    {
        Cache::forever(self::KEY_PRESENTATION, null);
    }

}