<?php

namespace App\Http\Controllers\Admin;

use App\Competition\Competition;
use App\Event\Presentation;
use App\Http\Controllers\Controller;
use App\Models\Competitor;
use App\Models\Event;
use Request;

class EventController extends Controller {

    public function index()
    {
        $events = Event::orderBy('num', 'asc')
            ->orderBy('name', 'asc')
            ->get();

        return view('admin.event.index', compact('events'));
    }

    /**
     * Set event undergoing prize presentation.
     * @param App\Models\Event $event
     */
    public function presenting(Event $event)
    {
        Presentation::presenting($event->id);
        return back();
    }

    public function clearPresentation()
    {
        Presentation::clear();
        return back();
    }

    /**
     * @param App\Models\Event $event
     */
    public function show(Event $event)
    {
        $competitors = $event->competitors()->get();

        return view('admin.event.show', compact('event', 'competitors'));
    }

    /**
     * @param App\Models\Event $event
     */
    public function callEvent(Event $event)
    {
        $event->call();
        return back();
    }

    /**
     * @param App\Models\Event $event
     */
    public function uncallEvent(Event $event)
    {
        $event->uncall();
        return back();
    }

    /**
     * Save ranking.
     * @param App\Models\Event $event
     */
    public function updateRank(Event $event)
    {
        $ranks = [];

        for ($i = 0; $i < count(Request::input('num')); $i++)
        {
            $num = Request::input("num.{$i}");
            if (!$num)
                continue;

            $competitor = Competitor::where('num', $num)->first();
            if (!$competitor)
                continue;

            $ranks[$competitor->id] = ['rank' => Request::input("rank.{$i}")];
        }

        if (count($ranks))
        {
            $event->competitors()
                ->sync($ranks);
        }

        return redirect()->route('event.show', [$event]);
    }

    /**
     * NOT USED
     * Remove competitor from event.
     * @param Competitor $competitor
     * @param Event $event
     */
    public function removeCompetitor(Event $event, Competitor $competitor)
    {
        Competition::unlinkEventCompetitor($event, $competitor);
        return back();
    }

    /**
     * NOT USED
     * Add competitor to event.
     * @param Competitor $competitor
     * @param Event $event
     */
    public function addCompetitor(Event $event)
    {
        $competitor = Competitor::where('num', Request::input('competitor_num'))->first();

        if ($competitor)
        {
            Competition::linkEventCompetitor($event, $competitor);
        }

        return back();
    }

}