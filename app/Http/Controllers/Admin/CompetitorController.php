<?php

namespace App\Http\Controllers\Admin;

use App\Competition\Competition;
use App\Http\Controllers\Controller;
use App\Models\Competitor;
use App\Models\Event;
use Request;

class CompetitorController extends Controller {

    /**
     * Searchable, listing of competitors.
     */
    public function index()
    {
        $competitors = Competitor::orderBy('name', 'asc')
            ->orderBy('num', 'asc')
            ->paginate();

        return view('admin.competitor.index', compact('competitors'));
    }


    public function create()
    {
        return view('admin.competitor.create');
    }

    public function store(Request $request)
    {
        $competitor = new Competitor([
            'name' => $request->name,
            'num' => $request->num,
        ]);

        $competitor->save();
        return redirect()->route('competitor.show', $competitor);
    }

    public function show(Competitor $competitor)
    {
        $events = $competitor->events()->orderBy('num', 'asc')->get();
        return view('admin.competitor.show', compact('competitor', 'events'));
    }

    public function edit(Request $request, Competitor $competitor)
    {

    }

    public function destroy(Competitor $competitor)
    {

    }

}