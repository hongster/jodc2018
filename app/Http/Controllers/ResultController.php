<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Request;

class ResultController extends Controller {

    public function index()
    {
        $events = Event::orderBy('num', 'asc')
            ->orderBy('name', 'asc')
            ->get()
            ->sort(function ($eventA, $eventB) {
                $numA = (int)($eventA->num);
                $numB = (int)($eventB->num);

                if ($numA == $numB)
                    return strcmp($eventA->num, $eventB->num);

                return $numA - $numB;
            });
        return view('result.index', compact('events'));
    }

    public function event(Event $event)
    {
        $rankedCompetitors = $event->competitors()
            ->get()
            ->sortBy('pivot.rank');
        return view('result.event', compact('event', 'rankedCompetitors'));
    }


}