<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Event\Facades\Calling;
use App\Event\Presentation;

class EventController extends Controller {

    /**
     * Output list of calling events with its competitors.
     */
    public function callingEvents()
    {
        $output = [
            'events' => Event::with('competitors')->find(Calling::events())
        ];

        return response()->json($output);
    }

    /**
     * Output event, and related competitors & ranking.
     */
    public function presentation()
    {
        $output = ['event' => Event::with('competitors')->find(Presentation::event())];

        return response()->json($output);
    }

}