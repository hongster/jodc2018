<?php

namespace App\Competition;

use App\Models\Competitor;
use App\Models\Event;

/**
 * Business logic for managing competitions.
 */
class Competition {

    /**@const Represent finalist (e.g. semi-final event) */
    const RANK_FINALIST = 99;

    /**
     * Add competitor to event.
     * @param Event $event
     * @param Competitor $competitor
     */
    public static function linkEventCompetitor(Event $event, Competitor $competitor)
    {
        $event->competitors()->syncWithoutDetaching([$competitor->id]);
    }

    /**
     * Remove competitor from event.
     * @param Event $event
     * @param Competitor $competitor
     */
    public static function unlinkEventCompetitor(Event $event, Competitor $competitor)
    {
        $event->competitors()->detach($competitor);
    }

    /**
     * Set competitor as finalist for an event.
     * @param Event $event
     * @param Competitor $competitor
     */
    public static function setFinalist(Event $event, Competitor $competitor)
    {
        return $event->competitors()->updateExistingPivot($competitor->id, ['rank' => self::RANK_FINALIST]);
    }

    /**
    * Set unset ranking.
    * @param Event $event
    * @param Competitor $competitor
    */
    public static function unrank(Event $event, Competitor $competitor)
    {
        return $event->competitors()->updateExistingPivot($competitor->id, ['rank' => null]);
    }

    /**
    * Set ranking for competitor in an event.
    * @param Event $event
    * @param Competitor $competitor
    * @param int $rank
    */
    public static function setRank(Event $event, Competitor $competitor, $rank)
    {
        return $event->competitors()->updateExistingPivot($competitor->id, ['rank' => $rank]);
    }

}