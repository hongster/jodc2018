<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as LaravelModel;

class Model extends LaravelModel {

    /**
     * Use UNIX timestamp by default. 
     * @var bool 
     */
    protected $dateFormat = 'U';

}