<?php

namespace App\Models;

use App\Models\Model;

class Competitor extends Model {
    
    /**
     * Event participant relationships.
     */
    public function events()
    {
        return $this->belongsToMany('App\Models\Event')->withPivot('rank');
    }

}