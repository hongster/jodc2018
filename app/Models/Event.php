<?php

namespace App\Models;

use App\Models\Model;
use App\Event\Facades\Calling;

class Event extends Model {
    
    /**
     * Event participant relationship.
     */
    public function competitors()
    {
        return $this->belongsToMany('App\Models\Competitor')->withPivot('rank');
    }

    /**
     * Determine if this event is in calling list.
     * @return bool
     */
    public function isCalling()
    {
        return Calling::isCallingEvent($this->id);
    }

    /**
     * Add this event to calling list.
     */
    public function call()
    {
        return Calling::call($this->id);
    }

    /**
     * Remove this event from calling list.
     */
    public function uncall()
    {
        return Calling::uncall($this->id);
    }

}