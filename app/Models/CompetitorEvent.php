<?php

namespace App\Models;

use App\Models\Model;

/**
 * Immediate table between `competitors` and `events`. Contain ranking.
 * *NOTE* on ranking:
 * - NULL and 0 - no ranking
 * - 99 - Finalist
 */
class CompetitorEvent extends Model {

    /** @var bool No timestamp. */
    public $timestamps = false;

}