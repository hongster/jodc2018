<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page/index');
});

Route::prefix('ajax')->namespace('Ajax')->group(function() {
    Route::get('event/callingEvents', 'EventController@callingEvents');
    Route::get('event/presentation', 'EventController@presentation');
});

Route::prefix('admin')->namespace('Admin')->group(function() {
    Route::get('/', function () {
        return view('admin.index');
    });

    Route::get('event/clearPresentation', 'EventController@clearPresentation')
        ->name('event.clearPresentation');
    
    Route::resource('event', 'EventController');
    Route::get('event/{event}/call', 'EventController@callEvent')
        ->name('event.call');
    Route::get('event/{event}/uncall', 'EventController@uncallEvent')
        ->name('event.uncall');
    Route::post('event/{event}/updateRank', 'EventController@updateRank')
        ->name('event.updateRank');

    Route::get('event/{event}/presenting', 'EventController@presenting')
        ->name('event.presenting');

    /* UNUSED */
    Route::post('event/{event}/addCompetitor', 'EventController@addCompetitor')
        ->name('event.addCompetitor');
    Route::get('event/{event}/removeCompetitor/{competitor}', 'EventController@removeCompetitor')
        ->name('event.removeCompetitor');

    Route::resource('competitor', 'CompetitorController');
    
});

Route::get('results', 'ResultController@index')
    ->name('result');
Route::get('results/event/{event}', 'ResultController@event')
    ->name('result.event');

// Catch all
Route::get('/{page}', function ($page) {
    $page = str_replace('-', '_', $page);
    return view("page.{$page}");
});