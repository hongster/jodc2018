@extends('layouts.app')

@section('title', 'Calling Competitors')

@section('content')
    <div id="app" class="text-center">
        <h1 class="text-danger d-none d-sm-block">Calling Competitors</h1>
        <h3 class="text-danger d-sm-none">Calling Competitors</h3>

        <div v-for="event in events">
            <div class="display-4 d-none d-sm-block">@{{ event.num }}. @{{ event.name }}</div>
            <h4 class="d-sm-none">@{{ event.num }}. @{{ event.name }}</h4>

            <p class="display-4 d-none d-sm-block mb-4"><span v-for="competitor in event.competitors" class="badge badge-primary mr-3">@{{ competitor.num }}</span></p>
            <h4 class="d-sm-none mb-4"><span v-for="competitor in event.competitors" class="badge badge-primary mr-3">@{{ competitor.num }}</span></h4>
        </div>
    </div>

    <div class="text-muted text-center mt-2">This page automatically updates itself. Please do not refresh this page.</div>
@endsection

@push('footer_scripts')
    <script src="{{ asset('js/callingEvents.js') }}"></script>
@endpush