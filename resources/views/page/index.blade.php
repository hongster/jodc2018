@extends('layouts.app')

@section('title', 'Welcome')

@section('content')

    <p><img src="{{ asset('img/jodc2018_backdrop.jpg') }}" class="img-fluid" alt="Johor Open Dance Championship 2018" title="Johor Open Dance Championship 2018"></p>

    <p class="lead">Welcome to JODC 2018! This competition is brought to you by <a href="https://www.facebook.com/SA-Dance-Academy-377952982222127" title="SA Dance Academy">SA Dance Academy</a>,
        a member of <a href="https://www.facebook.com/MalaysiaBallroomDanceCouncil/" title="MBDC">MBDC</a> (Malaysia Ballroom Dance Council). To all competitors, I wish you best of luck!
    </p>

@endsection