@extends('layouts.app')

@section('title', 'Prize Presentation')

@section('content')
    <div id="app" class="text-center">
        <h1 class="text-danger d-none d-sm-block">Prize Presentation</h1>
        <h3 class="text-danger d-sm-none">Prize Presentation</h3>

        <div v-if="event.competitors">
            <h1 class="d-none d-sm-block mb-4">@{{ event.name }}</h1>
            <h3 class="d-sm-none mb-4">@{{ event.name }}</h3>

            <div v-for="competitor in rankedCompetitors">
                <h3 class="d-none d-sm-block">@{{ competitor.pivot.rank }} - @{{ competitor.name }}</h3>
                <h4 class="d-sm-none">@{{ competitor.pivot.rank }} - @{{ competitor.name }}</h4>
            </div>
        </div>
    </div>

    <div class="text-muted text-center mt-2">This page automatically updates itself. Please do not refresh this page.</div>
@endsection

@push('footer_scripts')
    <script src="{{ asset('js/prizePresentation.js') }}"></script>
@endpush