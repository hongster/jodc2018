@extends('layouts.app')

@section('title', 'Competitors')

@section('content')
    <h1>Competitors</h1>

    <table class="table table-hover">
        <thead>
            <tr>
                <th>Number</th>
                <th>Name</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($competitors as $competitor)
                <tr>
                    <td>{{ $competitor->num }}</td>
                    <td>{{ link_to_route('competitor.show', $competitor->name, [$competitor]) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $competitors->links() }}
@endsection