@extends('layouts.app')

@section('title', 'New Competitor')

@section('content')
    <h1>New Competitor</h1>

    <div class="row">
        <div class="col-6">
            {{ Form::open(['route' => ['competitor.store']]) }}

                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('num', 'Tag Number') }}
                    {{ Form::text('num', null, ['class' => 'form-control']) }}
                    <small class="form-text text-muted">Leave this blank if you want system to auto-generate tag number.</small>
                </div>

                {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}

            {{ Form::close() }}
        </div>
    </div>
@endsection