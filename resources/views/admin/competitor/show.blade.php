@extends('layouts.app')

@section('title', $competitor->name)

@section('content')
    <h1>{{ $competitor->name }}</h1>

    <h2>Events</h2>

    @foreach ($events as $event)
        <h4>{{ link_to_route('event.show', "{$event->num}. {$event->name}", [$event]) }}</h4>
    @endforeach
    
@endsection