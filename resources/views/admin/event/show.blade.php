@extends('layouts.app')

@section('title', $event->name)

@section('content')
    <h1>{{ $event->name }}</h1>

    <h2>Competitors</h2>

    {{ Form::open(['route' => ['event.updateRank', $event]]) }}
        <table class="table" id="app">
            <thead>
                <tr>
                    <th>Rank</th>
                    <th>Competitor Number</th>
                </tr>
            </thead>

            @foreach ($competitors as $competitor)
                <tr>
                    <td>
                        <div class="form-group">
                            {{ Form::select('rank[]',
                                ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' =>'8', '9' => '9', '10' => '10', '99' => 'Finalist'], 
                                $competitor->pivot->rank,
                                ['class' => 'form-control']) }}
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="123" value="{{ $competitor->num }}" name="num[]">
                        </div>
                    </td>
                </tr>
            @endforeach

            @for ($i = count($competitors); $i < 10; $i++)
                <tr>
                    <td>
                        <div class="form-group">
                            <select class="form-control" name="rank[]">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="99">Finalist</option>
                            </select>
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="123" value="" name="num[]">
                        </div>
                    </td>
                </tr>
            @endfor
        </table>

        <button type="submit" class="btn btn-primary">Save Ranking</button>
    {{ Form::close() }}
    
@endsection