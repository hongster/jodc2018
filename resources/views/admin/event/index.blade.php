@extends('layouts.app')

@section('title', 'Events')

@section('content')
    <h1>Events</h1>

    @foreach ($events as $event)
        <div class="event-list-item">
            <h3>{{ link_to_route('event.show', "{$event->num}. {$event->name}", [$event]) }}</h3>

            <span>
                @if ($event->isCalling())
                    {{ link_to_route('event.uncall', "Calling Event", [$event], ['class' => 'badge badge-success']) }}
                @else
                    {{ link_to_route('event.call', "Calling Event", [$event]) }}
                @endif
            </span>
            | 
            <span>
                @if (App\Event\Presentation::isPresenting($event->id))
                    {{ link_to_route('event.clearPresentation', "Presenting Prize Now", [], ['class' => 'badge badge-warning']) }}
                @else
                    {{ link_to_route('event.presenting', "Presenting Prize Now", [$event]) }}
                @endif
            </span>
        </div>
    @endforeach
@endsection