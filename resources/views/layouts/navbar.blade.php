<div class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container">
    <a class="navbar-brand" href="{{ url('/') }}">JODC2018</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="main-navbar">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/') }}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href=" {{ url('calling-events') }} ">Calling Events</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('prize-presentation') }}">Prize Presentation</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('result') }}">Results</a>
        </li>

        {{-- Secret menu --}}
        @if (\Route::current()->getPrefix() == '/admin')
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Admin
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ url('admin/event') }}">Events</a>
              <a class="dropdown-item" href="{{ url('admin/competitor') }}">Competitors</a>
            </div>
          </li>
        @endif

      </ul>
    </div>
  </div>
</div>