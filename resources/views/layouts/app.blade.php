<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <title>@yield('title') | Johor Open Dance Championships 2018</title>
    @stack('header_scripts')
  </head>
  <body>
    @include('layouts.navbar')
    
    <div class="container" id="main-content">
      @yield('content')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    @stack('footer_scripts')
  </body>
</html>