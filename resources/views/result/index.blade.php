@extends('layouts.app')

@section('title', 'Results')

@section('content')
    <h1>Results</h1>

    @foreach ($events as $event)
        <h3 class="d-none d-sm-block"><a href="{{ route('result.event', [$event]) }}">{{ $event->num }}. {{ $event->name }}</a></h3>
        <div class="d-sm-none"><a href="{{ route('result.event', [$event]) }}">{{ $event->num }}. {{ $event->name }}</a></div>
    @endforeach
    
@endsection