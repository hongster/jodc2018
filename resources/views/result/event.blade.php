@extends('layouts.app')

@section('title', 'Event Result')

@section('content')
    <h1 class="d-none d-sm-block">{{ $event->num }}. {{ $event->name }}</h1>
    <h3 class="d-sm-none">{{ $event->num }}. {{ $event->name }}</h3>

    <p class="mt-4">
        @if (count($rankedCompetitors) == 0)
            No result yet...
        @endif

        @foreach ($rankedCompetitors as $competitor)
            <h4>{{ ($competitor->pivot->rank == 99) ? 'Finalist' : $competitor->pivot->rank }} - 
                {{ $competitor->name }}
            </h4>
        @endforeach
    </p>
@endsection