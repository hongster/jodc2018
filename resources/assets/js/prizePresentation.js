var app = new Vue({

    el: '#app',
    self: this,

    data: {
        event: {}
    },

    computed: {
        rankedCompetitors: function() {
            return this.event.competitors.sort(function(a, b) {
                return a.pivot.rank - b.pivot.rank;
            });
        }
    },

    mounted: function() {
        setInterval(function () {
            axios.get('/ajax/event/presentation')
            .then(function(response) {
                //console.log(response);

                app.event = response.data.event;
            })
            .catch(function (error) {
                console.log(error);
            });
        }, 2000);
    }

});