var app = new Vue({

    el: '#app',
    self: this,

    data: {
        events: []
    }

});

setInterval(function () {
    axios.get('/ajax/event/callingEvents')
        .then(function(response) {
            console.log(response);

            app.events = response.data.events;
        })
        .catch(function (error) {
            console.log(error);
        });
}, 2000);